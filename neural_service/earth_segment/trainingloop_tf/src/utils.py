import cv2
import numpy as np


def preprocess_image(image, config):
    image = cv2.resize(image, (config.img_size, config.img_size))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = image / 255.0
    image = np.array(image, dtype=np.float32)
    image = np.expand_dims(image, axis=0)

    return image
