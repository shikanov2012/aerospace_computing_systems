import tensorflow as tf
import tensorflow_addons as tfa
from src.dataset import get_dataloader
from src.fit import fit
from src.models import pretrain_model

from config.config import config

if __name__ == "__main__":
    train, val, n_classes = get_dataloader(config)

    # model = model_baseline(config, n_classes)
    model = pretrain_model(config, n_classes)

    model.summary()

    optimizer = tf.keras.optimizers.Adam(learning_rate=config.lr)
    loss = tfa.losses.SigmoidFocalCrossEntropy()

    metric_macto_av_f1 = tfa.metrics.F1Score(num_classes=n_classes, average="macro")
    metric_auc = tf.keras.metrics.AUC(
        multi_label=True, num_labels=n_classes, name="auc_ROC"
    )

    model.compile(
        optimizer=optimizer, loss=loss, metrics=[metric_macto_av_f1, metric_auc]
    )

    fit(config, model, train, val)
